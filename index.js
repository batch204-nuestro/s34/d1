const express = require('express');

const app = express();

app.use(express.json());

const port = 3000;

app.get("/", (req, res) =>{
	res.send('hello world');

})

app.post("/hello", (req,res) => {
	console.log(req.body);
	res.send(`hello there! ${req.body.firstName} ${req.body.lastName}!`);
});


// mock database for our users
	let users = [
		{
			username: "janedoe",
			password: "jane123"
		},
		{
			username: "johnsmith",
			password: "john123"
		}
	];

app.post ("/signup", (req, res) => {

		console.log(req.body);

		if(req.body.username !== "" && req.body.password !== "" && req.body.username !== undefined && req.body.password !== undefined)
		{

			users.push(req.body);

			res.send(`user ${req.body.username} successfully registered!`)	
		}

		else {
			res.send("please input both username and password");
		}
});


app.get("/users", (req, res) => {
	res.send(users);
})


//PUT
app.put("/change-password", (req, res) => {
	let message;

	for (let i=0; i < users.length; i++){
		if(users[i].username === req.body.username){
			users[i].password = req.body.password;

			message = `user ${req.body.username}'s password has been updated `
			break;
		}
		else {
			message = "user does not exist"
		}
	}

	res.send(message)

})

//delete

app.delete("/delete-user", (req, res) => {
	let message;

	for (let i=0; i < users.length; i++){
		if(users[i].username === req.body.username){
			users.splice(users[i], 1);

			message = `user ${req.body.username} has been deleted`
			break;
		}
		else {
			message = "user does not exist"
		}
	}

	res.send(message)

})

app.listen(port, () => console.log(`server is running at port ${port}` ))